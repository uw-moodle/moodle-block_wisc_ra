****************************************************************************************************
UW-Madison Moodle plugins
- AUTHOR: Nick Koeppen (nkoeppen@wisc.edu)
- UPDATE: 21 Feb 2012
****************************************************************************************************
- TYPE: block
- NAME: wisc_ra (WISC Roster Associations)
- REQUIRES:
 - enrol/wisc 
****************************************************************************************************
*****  Description   *****
This block is used to verify Roster Associations (RA) set in the WISC Enrollments (enrol/wisc)
plugin.  The block also provides forward links for editing a WISC enrollment instance and enrolling
additional users.

***** Default blocks *****
Moodle 2.x:

Default blocks in Moodle 2.x are hard coded in config.php by setting the defaultblocks_override
variable.  Refer to config-dist.php for more documentation.

- Format:
* The block names are in CSV (comma-separated values).
* The colon separates blocks on the left and right sides.

- Example:

$CFG->defaultblocks_override = 'participants,course_list:wisc_ra,news_items';