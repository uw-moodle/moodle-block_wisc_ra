<?php
/******************************************************************************
* UW Madison Roster Associations Block - Version Identifier
*
* Moodle code fragment to establish plugin version and cron cycle.
*
* Author: Nick Koeppen
******************************************************************************/
/**
 * Version details
 *
 * @package    block
 * @subpackage wisc_ra
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2017032400;
$plugin->component = 'block_wisc_ra';
$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = 'Fall2012';
$plugin->dependencies = array(
    'enrol_wisc' => 2012021401,
);
