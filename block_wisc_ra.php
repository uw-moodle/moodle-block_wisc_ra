<?php
/******************************************************************************
* UW Madison Roster Associations Block
*
* Display roster to Moodle associations and course information.
*
* Author: Nick Koeppen
******************************************************************************/

require_once($CFG->dirroot.'/enrol/wisc/controller.class.php');

class block_wisc_ra extends block_base {
	function init() {
	    global $OUTPUT;
	    $infoicon = $OUTPUT->help_icon('blockinfo', 'block_wisc_ra');
		$this->title = get_string('blocktitle', 'block_wisc_ra').' '.$infoicon;
	}

	function applicable_formats() {
	    // Default case: the block can be used in all course types
	    return array('site' => false, 'my' => false, 'course-view' => true);
	}

	function get_content() {
	    global $COURSE, $DB, $OUTPUT;
		if ($this->content !== NULL || $COURSE->id == SITEID || !has_capability('block/wisc_ra:view', $this->context)) {
			return $this->content;
		}
		//Only on course pages for people with capability
		$this->content = new stdClass();

		/* Retrieve and organize coursemap */
		$orderBy = 'term, catalog_number, section_number, subject_code';
		$dbSections = $DB->get_records('enrol_wisc_coursemap', array('courseid' => $COURSE->id), $orderBy);
		$sections = array();
		foreach($dbSections as $dbSect){
		    $isisid  = !empty($dbSect->isis_course_id) ? $dbSect->isis_course_id : $dbSect->class_number;
		    $subject = !empty($dbSect->subject) ? preg_replace('/ /','', $dbSect->subject) : $dbSect->subject_code;
		    $secttype = !empty($dbSect->type) ? $dbSect->type : 'SEC';
		    if (!isset($sections[$dbSect->term][$isisid])) {
		        $sections[$dbSect->term][$isisid] = array();
		        $sections[$dbSect->term][$isisid]['subjects'] = array();
		        $sections[$dbSect->term][$isisid]['catalognum'] = $dbSect->catalog_number;
		    }
		    $section = "$secttype $dbSect->section_number";
		    $sections[$dbSect->term][$isisid]['sections'][$section][] = $dbSect->subject_code;
		    $sections[$dbSect->term][$isisid]['subjects'][$dbSect->subject_code] = $subject;
		}

		$editurls = $this->get_edit_urls($COURSE->id);

		/* Display sections mapped */
		$html = '';
		if (!empty($sections)) {
			foreach($sections as $term => $courses){
			    $html .= $OUTPUT->heading($this->termname($term), 3, 'termname');
			    foreach($courses as $course){
			        //Form course name
			        $subjects   = implode('/', $course['subjects']);
			        $html .= $OUTPUT->heading("$subjects $course[catalognum]", 4, 'coursename');

			        $items = array();
			        $subjectcodes = array_keys($course['subjects']);
			        foreach($course['sections'] as $section => $sectionsubjs){
			            $item = $section;
			            if (array_diff($subjectcodes, $sectionsubjs)) {
			                $subjnames = array_intersect_key($course['subjects'], $sectionsubjs);
			                $item .= " (only ".implode(', ', $subjnames).")";
			            }
			            $items[] = $item;
			        }
			        $html .= html_writer::alist($items, array('class' => 'sections'));
			    }
			}
		} else {
		    $html .= $OUTPUT->notification(get_string('nosectionsfound', 'block_wisc_ra'));
		}

		/* Roster controls */
		if (has_capability('enrol/wisc:config', $this->context)) {
		    $items = array();

			//Additional users - enrolled users link
			$url   = new moodle_url('/enrol/users.php', array('id' => $COURSE->id));
			$label = get_string('addmoreusers', 'block_wisc_ra');
			$icon  = $OUTPUT->pix_icon('t/add', $label);
			$items[] = $OUTPUT->action_link($url, "$icon $label");

			//Edit roster associations - enrollment edit link
			foreach($editurls as $term => $url){
				$label   = get_string('editterm', 'block_wisc_ra', $this->termname($term));
				$icon    = $OUTPUT->pix_icon('t/edit', $label);
				$items[] = $OUTPUT->action_link($url, "$icon $label");
			}
            $html .= html_writer::alist($items, array('class' => 'controls'));
		}

		$this->content->text = $html;
		return $this->content;
	}

	function get_edit_urls($courseid){
		global $DB;

		$baseurl = new moodle_url('/enrol/wisc/instance.php', array('courseid' => $courseid));

		$urls = array();
		foreach(enrol_get_instances($courseid, true) as $instance){
			if ($instance->enrol != 'wisc') {
				continue;
			}
			$term = $instance->{enrol_wisc_plugin::termfield};
			if (empty($term)) {
			    continue;
			}
			$urls[$term] = new moodle_url($baseurl, array('id' => $instance->id));
		}

		return $urls;
	}

	function termname($term){
		$term = (string)$term;
		if (strlen($term) != 4) {
			return "Invalid term";
		}

		$century = (int)$term[0];
		$decades = (int)$term[1];
		$decyear = (int)$term[2];
		$yearsem = (int)$term[3];
		if ($century == 0) {
			$year = 1900 + $decades*10 + $decyear;
		} else {
			$year = 2000 + $decades*10 + $decyear;
		}

		switch($yearsem){
			case 2:
				$sem = "Fall";
				$year -= 1;		//This term belongs to the next academic school year
				break;
			case 3:
				$sem = "Winter";
				$year -= 1;		//This term belongs to the next academic school year
				break;
			case 4:
				$sem = "Spring";
				break;
			case 6:
				$sem = "Summer";
				break;
		}

		return $sem . ' ' . $year;
	}
}
