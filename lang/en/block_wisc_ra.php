<?php

$string['pluginname']     = 'WISC Roster Associations';
$string['blocktitle']     = 'UW Sections';
$string['blockinfo']      = 'UW Sections';
$string['blockinfo_help'] = 'This block is only visible to course managers and not students.  Use the contents to verify the UW sections mapping.';
$string['addmoreusers']   = 'Enroll additional users';
$string['editterm']       = 'Edit {$a} roster';

$string['wisc_ra:view'] = 'View WISC Roster Associations block';
$string['nosectionsfound'] = 'No sections mapped.';
